
use docu;

DROP PROCEDURE IF EXISTS `p_stock_goods_qty_calc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_stock_goods_qty_calc`(IN goods_id int, in stock_id int, in group_id int)
BEGIN
    #计算某个物料的当前库存，以最近的一个期次库存加上收发数量
    #OrderApply: 1: '采购申请单',
	#Order: 2: '采购订单',
	#DocuArrive: 3: '到货通知单',
	#DocuCheck: 4: '外购入库单',
	#DocuSale: 5: '销售出库单',
	#DocuPick: 6: '领料出库单',
	#DocuStock: 7: '盘点单',
	#DocuTransfer: 8: '调拨单'
	declare begin_time timestamp;
	declare begin_qty	decimal(12,3);
	declare sum_qty	decimal(12,3);
	select  c_end_qty,c_time_end into begin_qty,begin_time from vw_period_goods where c_goods=goods_id 
		and c_stock= stock_id and c_status=1 order by c_time_end desc limit 0,1;

	select sum(c_qty *(case when ((c_type =4 and c_stock=stock_id ) or (c_type=8 and c_stock_to=stock_id)) then 1 
										when (c_type in(5,6,8) and c_stock= stock_id) then -1 else 0 end ))
			into sum_qty
		from vw_docu_list where c_time >ifnull(begin_time,'2016-1-1') 
			and c_status =1213 and ((c_type in(4,5,6,8) and c_stock= stock_id) or (c_type = 8 and c_stock_to= stock_id)) 
			and c_goods= goods_id;
	#更新库存数量
	if exists(select * from t_stock_goods where c_stock= stock_id and c_goods= goods_id) then
		update t_stock_goods set c_qty=ifnull(begin_qty,0) +ifnull(sum_qty,0)
			where c_goods=goods_id and c_stock=stock_id;			
	else
		insert t_stock_goods(c_stock,c_goods,c_qty,c_group, c_qty_min,c_qty_max,c_price,c_user,c_time,c_memo)
			values( stock_id,goods_id,ifnull(begin_qty,0) +ifnull(sum_qty,0),group_id,  0,0,0,1,now(),'');
	end if;

	select begin_qty,sum_qty;

END
;;
DELIMITER ;


DROP PROCEDURE IF EXISTS `p_docu_rec_qty_from_calc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_docu_rec_qty_from_calc`(IN modulename varchar(20), in id int, in isDel int)
BEGIN
    #重新计算来源数量，设置是否关闭的标记
	declare sum_qty decimal(8,3) default 0;
	declare id_from	int;
	declare id_from_order int;
	declare docu_type	int;
	select id_from=c_rec_from,docu_type=c_type from vw_docu_list where rec_id=id;
	delete from t_docu_rec where c_id=id and isDel=1;
		
	select sum(c_qty) into sum_qty from vw_docu_list 
		where c_rec_from= id_from and c_type= docu_type and c_status >0;
	#原DocuArrive,现在跳过了收料单，直接从订单到外购入库单
	if modulename='DocuCheck' then
		update t_order_rec set c_close=(case when @sum_qty>=c_qty then 1 else 0 end),c_qty_to= sum_qty where c_id =id_from;
	elseif modulename='DocuBill' then
		update t_docu_rec set c_qty_kp= sum_qty where c_id =id_from;
		select c_rec_from into id_from_order from t_docu_rec where c_id=id_from;
		select sum(c_qty_kp) into sum_qty from vw_docu_list 
			where c_rec_from=id_from_order and c_type=807 and c_status >0;
		update t_order_rec set c_qty_kp=sum_qty where c_id=id_from_order;
	else
		update t_docu_rec set c_close=(case when sum_qty>=c_qty then 1 else 0 end),c_qty_to=sum_qty where c_id =id_from;
	end if;

END
;;
DELIMITER ;




DROP PROCEDURE IF EXISTS `p_order_rec_qty_from_calc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_order_rec_qty_from_calc`(in id int, in is_del int, in is_back int)
BEGIN
    #重新计算来源数量，设置是否关闭的标记
	declare id_from int;
	declare sum_qty decimal(8,3) default 0;
	select  c_rec_from into id_from from t_order_rec where c_id= id;
	delete from t_order_rec where c_id= id and  is_del=1;

	select  sum(c_qty) into sum_qty from t_order_rec A,t_order B 
		where A.c_order=B.c_id and A.c_rec_from= id_from and B.c_status >0
			and ifnull(B.c_is_back,0)= is_back;
			
	if is_back=1 then
		update t_docu_rec set c_close=0,c_qty_to= sum_qty where c_id = id_from;
	else
		update t_order_apply_rec set c_close=0,c_qty_to= sum_qty where c_id = id_from;
	end if;

END
;;
DELIMITER ;

DROP PROCEDURE IF EXISTS `p_period_amt_calc`;
DELIMITER ;;
CREATE DEFINER =`root`@`localhost` PROCEDURE `p_period_amt_calc`(in period_id int)
BEGIN
    #计算某个期次的合计数金额
	select @sum_begin:=sum(c_begin), @sum_in :=sum(c_in), @sum_out :=sum(c_out), @sum_end :=sum(c_end) 
		from t_period_goods where c_period = period_id;
	update t_period set c_begin = @sum_begin,c_in = @sum_in,c_out = @sum_out,c_end = @sum_end
		where c_id = period_id;

END
;;
DELIMITER ;

DROP PROCEDURE IF EXISTS `p_period_goods_calc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_period_goods_calc`(IN period_id int, in stock_id int)
BEGIN
    #按期次仓库的库存结转
	declare last_period_id	int;
	declare time_begin	timestamp;
	declare time_end	timestamp;	
	select max(c_id) into last_period_id  from t_period where c_stock=@stock_id and c_status=1;
	select c_time_begin,c_time_end into time_begin,time_end from t_period where c_id = period_id;
	#删除当前期次的结转数据
	delete from t_period_goods where c_period= period_id;
	#加入当前所有物料
	insert t_period_goods(c_period,c_stock,c_goods, c_begin,c_begin_qty,c_begin_price,c_end,c_end_qty,c_end_price,c_in,c_in_qty,c_in_price,c_out,c_out_qty,c_out_price,c_memo)
		select period_id,stock_id, c_id, 0,0,0, 0,0,0, 0,0,0, 0,0,0, ''
		from t_goods where c_type=0 and c_status =0;
	
	#取上次该仓库的期次结转数据
	update t_period_goods inner join t_period_goods B
		on t_period_goods.c_stock=B.c_stock 
			and t_period_goods.c_goods=B.c_goods
		set c_begin =B.c_end, c_begin_price=ifnull(B.c_end_price,0), c_begin_qty = ifnull(B.c_end_qty,0)
        where t_period_goods.c_period= period_id;

	
	#统计物料的出入库数量
    CREATE TEMPORARY TABLE tmp_goods
	select c_goods, sum(case when (A.c_type in(807,808) and A.c_stock=stock_id ) or (A.c_type=1519 and A.c_stock_to=stock_id) then c_qty else 0 end) as qtyIn,
			sum(case when (A.c_type in(809,810,1519,1862) and A.c_stock=stock_id)  then c_qty else 0 end) as qtyOut
		from  t_docu A, t_docu_rec B 
		where A.c_id=B.c_docu and (c_stock=stock_id or c_stock_to=stock_id)  and c_status=836
			and A.c_time > time_begin and A.c_time <time_end
		group by c_goods;
	update t_period_goods inner join tmp_goods B
    on t_period_goods.c_goods=B.c_goods
        set c_in_qty=B.qtyIn,c_out_qty=B.qtyOut
		where c_period= period_id;
	update t_period_goods set c_begin_price=0 where c_begin_price is null and c_period= period_id;
	update t_period_goods set c_end_qty= c_begin_qty +c_in_qty -c_out_qty,c_end_price=c_begin_price,c_end= (c_begin_qty +c_in_qty -c_out_qty)*c_begin_price,
			c_end_qty_calc= c_begin_qty +c_in_qty -c_out_qty
		where c_period= period_id;
	#删除数量全为0的记录
	delete from t_period_goods where c_period= period_id and c_end_qty=0 and c_in_qty=0 and c_out_qty=0 and c_begin_qty=0
		and c_end=0 and c_in=0 and c_out=0 and c_begin=0;	
	
	#取物料的最新价格
	update t_period_goods inner join t_price_stock B
    on t_period_goods.c_goods=B.c_goods 
        set c_price=B.c_price
		where  B.c_stock= stock_id and t_period_goods.c_period= period_id;

	drop table tmp_goods;
	select period_id as period_id;

	#需要重新计算进价，加权平均法


END
;;
DELIMITER ;



DROP PROCEDURE IF EXISTS `p_period_stock_goods_qty_calc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_period_stock_goods_qty_calc`(IN period_id int)
BEGIN
    #根据某个期次重新计算当前库存
	#OrderApply: 1: '采购申请单',
	#Order: 2: '采购订单',
	#DocuArrive: 3: '到货通知单',
	#DocuCheck: 4: '外购入库单',
	#DocuSale: 5: '销售出库单',
	#DocuPick: 6: '领料出库单',
	#DocuStock: 7: '盘点单',
	#DocuTransfer: 8: '调拨单'
	declare begin_time timestamp;
	declare stock_id	int;
	declare group_id	int;
	select  c_time_end,c_stock into begin_time,stock_id from t_period where c_id=period_id;
	select c_group into group_id  from vw_stock where c_id=stock_id;
    create temporary table tmp_end
		select c_goods,c_end_qty 
			from t_period_goods 
			where c_period =period_id;
	create temporary table tmp_inout
		select c_goods, sum(c_qty *(case when (c_type in(4,7,8) and c_stock=stock_id ) then 1 
			when (c_type in(5,6) or (c_type=8 and c_stock_to=stock_id)) then -1 else 0 end )) as sumQty
			from vw_docu_list where c_time >begin_time and c_stock=stock_id 
				and c_status=1213 and ((c_type in(4,5,6,7,8) and c_stock=stock_id)or (c_type=8 and c_stock_to=stock_id))
			group by c_goods;
            
	#加上调入的库存	
    update tmp_end inner join tmp_inout
    on tmp_end.c_goods = tmp_inout.c_goods
    set c_end_qty = tmp_end.c_end_qty + tmp_inout.sumQty;

	insert tmp_end(c_goods,c_end_qty)
		select c_goods,sumQty
			from tmp_inout 
			where c_goods not in(select c_goods from tmp_end);
	
	#更新库存数量
	update t_stock_goods set c_qty=0 where c_stock=stock_id ;
	update t_stock_goods inner join tmp_end B
    on t_stock_goods.c_goods=B.c_goods and t_stock_goods.c_stock=stock_id
		set c_qty= c_end_qty;
	insert t_stock_goods(c_stock,c_goods,c_qty,c_qty_min,c_qty_max,c_memo,c_group)
		select stock_id,c_goods,c_end_qty, 0,0,'',group_id
		from tmp_end
		where c_goods not in(select c_goods from t_stock_goods where c_stock=stock_id);

END
;;
DELIMITER ;


-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: docu
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.17.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_docu`
--

DROP TABLE IF EXISTS `t_docu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_docu` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `c_no` varchar(20) NOT NULL,
  `c_type` int(10) NOT NULL,
  `c_way` int(10) NOT NULL,
  `c_supplier` int(10) NOT NULL,
  `c_stock` int(10) NOT NULL,
  `c_type_from` int(10) NOT NULL,
  `c_manager` int(10) NOT NULL,
  `c_salesman` int(10) NOT NULL,
  `c_jizhang` int(10) NOT NULL,
  `c_baoguan` int(10) NOT NULL,
  `c_user` int(10) NOT NULL,
  `c_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `c_dept` int(10) NOT NULL,
  `c_status` int(10) NOT NULL,
  `c_is_back` bit(1) NOT NULL,
  `c_appr_people` int(10) NOT NULL,
  `c_appr_time` timestamp(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000',
  `c_no_from` varchar(20) NOT NULL,
  `c_no_order` varchar(20) NOT NULL,
  `c_kp_type` int(10) NOT NULL,
  `c_pay_type` int(10) NOT NULL,
  `c_stock_to` int(10) NOT NULL,
  `c_period` int(10) NOT NULL,
  `c_amt_should` decimal(18,2) NOT NULL,
  `c_amt_actual` decimal(18,2) NOT NULL,
  `c_memo` varchar(255) NOT NULL,
  `c_group` int(10) NOT NULL,
  `c_act` int(10) NOT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `PK_T_DOCU` (`c_id`),
  KEY `idx_docu_group` (`c_group`),
  KEY `idx_docu_status` (`c_status`),
  KEY `idx_docu_stock` (`c_stock`),
  KEY `idx_docu_supplier` (`c_supplier`),
  KEY `idx_docu_time` (`c_time`),
  KEY `idx_docu_user` (`c_user`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_docu`
--

LOCK TABLES `t_docu` WRITE;
/*!40000 ALTER TABLE `t_docu` DISABLE KEYS */;
INSERT INTO `t_docu` VALUES (2,'GR201712130001',4,1,2,671,0,2,1,0,0,1,'2017-12-14 09:18:02.000000',1207,1213,'\0',1,'2017-12-14 09:18:02.000000','','',0,0,0,0,0.00,0.00,'',2,131),(3,'XC201712130001',5,1,2,667,0,2,1,0,0,1,'2017-12-13 06:01:18.000000',1207,1211,'\0',0,'2017-12-13 06:01:18.000000','','',0,0,0,0,0.00,0.00,'',2,116),(4,'XC201712130002',5,1,2,667,0,2,1,0,0,1,'2017-12-13 06:46:49.000000',1207,1212,'\0',1,'2017-12-13 06:46:49.000000','','',0,0,0,0,0.00,0.00,'',2,117),(5,'GR201712130002',4,1,1,667,0,2,1,0,0,1,'2017-12-13 09:37:47.000000',1207,1211,'',0,'2017-12-13 09:37:47.000000','','',0,0,0,0,0.00,0.00,'',2,123);
/*!40000 ALTER TABLE `t_docu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_docu_rec`
--

DROP TABLE IF EXISTS `t_docu_rec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_docu_rec` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `c_docu` int(10) NOT NULL,
  `c_goods` int(10) NOT NULL,
  `c_unit` int(10) NOT NULL,
  `c_qty` decimal(8,3) NOT NULL,
  `c_price` decimal(12,6) NOT NULL,
  `c_price_tax` decimal(12,6) NOT NULL,
  `c_tax` decimal(5,2) NOT NULL,
  `c_amt` decimal(18,2) NOT NULL,
  `c_amt_tax` decimal(18,2) NOT NULL,
  `c_memo` varchar(255) NOT NULL,
  `c_price_out` decimal(12,6) NOT NULL,
  `c_price_out_tax` decimal(12,6) NOT NULL,
  `c_amt_out` decimal(18,2) NOT NULL,
  `c_amt_out_tax` decimal(18,2) NOT NULL,
  `c_qty_from` decimal(8,3) NOT NULL,
  `c_qty_to` decimal(8,3) NOT NULL,
  `c_qty_stock` decimal(8,3) NOT NULL,
  `c_rec_from` int(10) NOT NULL,
  `c_no_from` varchar(20) NOT NULL,
  `c_no_order` varchar(20) NOT NULL,
  `c_qty_kp` decimal(8,3) NOT NULL,
  `c_close` bit(1) NOT NULL,
  `c_supplier` int(10) NOT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `PK_T_DOCU_REC` (`c_id`),
  KEY `idx_docu_rec_master` (`c_docu`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_docu_rec`
--

LOCK TABLES `t_docu_rec` WRITE;
/*!40000 ALTER TABLE `t_docu_rec` DISABLE KEYS */;
INSERT INTO `t_docu_rec` VALUES (4,2,4,1224,5.000,0.000000,0.000000,17.00,0.00,0.00,'',0.000000,0.000000,0.00,0.00,5.000,0.000,0.000,3,'PO201712060001','PO201712060001',0.000,'\0',1),(5,2,5,1224,1.000,0.000000,0.000000,17.00,0.00,0.00,'',0.000000,0.000000,0.00,0.00,1.000,0.000,0.000,2,'PO201712060001','PO201712060001',0.000,'\0',1),(6,4,6,688,1.000,0.000000,0.000000,17.00,0.00,0.00,'',0.000000,0.000000,0.00,0.00,1.000,0.000,0.000,4,'PO201712060002','PO201712060002',0.000,'\0',2),(9,4,5,1224,1.000,0.000000,0.000000,17.00,0.00,0.00,'',0.000000,0.000000,0.00,0.00,0.000,0.000,0.000,0,'','',0.000,'\0',0);
/*!40000 ALTER TABLE `t_docu_rec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_goods`
--

DROP TABLE IF EXISTS `t_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_goods` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `c_ucode` varchar(50) NOT NULL,
  `c_name` varchar(255) NOT NULL,
  `c_desc` varchar(255) NOT NULL,
  `c_property` int(10) NOT NULL,
  `c_class` int(10) NOT NULL,
  `c_unit` int(10) NOT NULL,
  `c_type` int(10) NOT NULL,
  `c_default_warehouse` int(10) NOT NULL,
  `c_default_position` int(10) NOT NULL,
  `c_default_supplier` int(10) NOT NULL,
  `c_source` int(10) NOT NULL,
  `c_min_stock` decimal(8,3) NOT NULL,
  `c_max_stock` decimal(8,3) NOT NULL,
  `c_status` int(10) NOT NULL,
  `c_cost` decimal(12,6) NOT NULL,
  `c_pic` varchar(255) NOT NULL,
  `c_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `PK_T_GOODS` (`c_id`),
  KEY `idx_goods_class` (`c_class`),
  KEY `idx_goods_no` (`c_ucode`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_goods`
--

LOCK TABLES `t_goods` WRITE;
/*!40000 ALTER TABLE `t_goods` DISABLE KEYS */;
INSERT INTO `t_goods` VALUES (4,'0101','广告牌','xxx1223xxxxxx',0,707,1224,0,0,0,0,0,0.000,0.000,0,0.000000,'/static/upfiles/t_goods/2017/loginbg_05.jpg',''),(5,'0401','钉子','kdkdkd',0,707,1224,0,0,0,0,0,0.000,0.000,0,0.000000,'/static/upfiles/t_goods/2017/loginbg_07.jpg',''),(6,'0301','电缆001','001xxxxxx',0,709,688,0,0,0,0,0,0.000,0.000,0,0.000000,'/static/upfiles/t_goods/2017/loginbg_04.jpg','');
/*!40000 ALTER TABLE `t_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_order`
--

DROP TABLE IF EXISTS `t_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_order` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `c_no` varchar(20) NOT NULL,
  `c_type` int(10) DEFAULT NULL,
  `c_way` int(10) DEFAULT NULL,
  `c_supplier` int(10) NOT NULL,
  `c_type_from` int(10) DEFAULT NULL,
  `c_addr_delivery` int(10) NOT NULL,
  `c_consignee` varchar(50) NOT NULL,
  `c_cash_type` int(10) NOT NULL,
  `c_currency` int(10) DEFAULT NULL,
  `c_exchange_rate` decimal(5,2) DEFAULT NULL,
  `c_manager` int(10) DEFAULT NULL,
  `c_salesman` int(10) DEFAULT NULL,
  `c_user` int(10) NOT NULL,
  `c_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `c_dept` int(10) NOT NULL,
  `c_status` int(10) DEFAULT NULL,
  `c_memo` varchar(255) DEFAULT NULL,
  `c_is_back` bit(1) DEFAULT NULL,
  `c_group` int(10) DEFAULT NULL,
  `c_appr_people` int(10) DEFAULT NULL,
  `c_appr_time` timestamp(6) NULL DEFAULT NULL,
  `c_no_from` varchar(20) DEFAULT NULL,
  `c_no_order` varchar(20) DEFAULT NULL,
  `c_guarantee` int(10) DEFAULT NULL,
  `c_content` varchar(225) DEFAULT NULL,
  `c_amt_should` decimal(18,2) DEFAULT NULL,
  `c_amt_actual` decimal(18,2) DEFAULT NULL,
  `c_act` int(10) DEFAULT NULL,
  UNIQUE KEY `c_id` (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_order`
--

LOCK TABLES `t_order` WRITE;
/*!40000 ALTER TABLE `t_order` DISABLE KEYS */;
INSERT INTO `t_order` VALUES (1,'PO201712060001',2,1,1,0,795,'',798,NULL,NULL,2,1,1,'2017-12-12 07:55:22.000000',1207,1213,'dsss',NULL,2,1,'2017-12-12 07:55:22.000000',NULL,NULL,NULL,NULL,NULL,NULL,113),(2,'PO201712060002',2,1,2,0,795,'',798,NULL,NULL,2,1,1,'2017-12-06 08:35:28.000000',1207,1212,'',NULL,2,1,'2017-12-06 08:35:28.000000',NULL,NULL,NULL,NULL,NULL,NULL,108);
/*!40000 ALTER TABLE `t_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_order_apply`
--

DROP TABLE IF EXISTS `t_order_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_order_apply` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `c_no` varchar(20) NOT NULL,
  `c_dept` int(10) NOT NULL,
  `c_way` int(10) NOT NULL,
  `c_appr_people` int(10) NOT NULL,
  `c_appr_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `c_applicant` int(10) NOT NULL,
  `c_applicat_time` timestamp(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000',
  `c_group` int(10) NOT NULL,
  `c_user` int(10) NOT NULL,
  `c_status` int(10) NOT NULL,
  `c_time` timestamp(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000',
  `c_memo` varchar(255) NOT NULL,
  `c_act` int(10) DEFAULT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `PK_T_ORDER_APPLY` (`c_id`),
  KEY `idx_order_apply_group` (`c_group`),
  KEY `idx_order_apply_time` (`c_time`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_order_apply`
--

LOCK TABLES `t_order_apply` WRITE;
/*!40000 ALTER TABLE `t_order_apply` DISABLE KEYS */;
INSERT INTO `t_order_apply` VALUES (4,'PR201712050001',1207,1,0,'2017-12-05 08:47:01.000000',2,'2017-12-05 08:47:01.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(5,'PR201712050002',1207,1,0,'2017-12-05 08:54:59.000000',1,'2017-12-05 08:54:59.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(6,'PR201712050003',1207,1,0,'2017-12-05 08:57:41.000000',1,'2017-12-05 08:57:41.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(7,'PR201712050004',1207,1,0,'2017-12-05 08:58:41.000000',1,'2017-12-05 08:58:41.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(8,'PR201712050005',1207,1,0,'2017-12-05 08:59:15.000000',1,'2017-12-05 08:59:15.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(9,'PR201712050006',1207,1,0,'2017-12-05 09:05:23.000000',1,'2017-12-05 09:05:23.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(10,'PR201712050007',1207,1,0,'2017-12-05 09:10:34.000000',1,'2017-12-05 09:10:34.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(11,'PR201712050008',1207,1,0,'2017-12-05 09:16:23.000000',1,'2017-12-05 09:16:23.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(12,'PR201712050009',1207,1,0,'2017-12-05 09:18:57.000000',1,'2017-12-05 09:18:57.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(13,'PR201712050010',1207,1,0,'2017-12-05 09:18:57.000000',1,'2017-12-05 09:18:57.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(14,'PR201712050010',1207,1,0,'2017-12-05 09:18:57.000000',1,'2017-12-05 09:18:57.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(15,'PR201712050011',1207,1,0,'2017-12-05 09:18:57.000000',1,'2017-12-05 09:18:57.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(16,'PR201712050010',1207,1,0,'2017-12-05 09:18:57.000000',1,'2017-12-05 09:18:57.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(17,'PR201712050012',1207,1,0,'2017-12-05 09:18:57.000000',1,'2017-12-05 09:18:57.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(18,'PR201712050013',1207,1,0,'2017-12-05 09:35:32.000000',1,'2017-12-05 09:35:32.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(19,'PR201712050014',1207,1,0,'2017-12-05 09:38:00.000000',1,'2017-12-05 09:38:00.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(20,'PR201712050015',1207,1,0,'2017-12-05 09:40:24.000000',1,'2017-12-05 09:40:24.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(21,'PR201712050010',1207,1,0,'2017-12-05 09:18:57.000000',1,'2017-12-05 09:18:57.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(22,'PR201712050016',1207,1,0,'2017-12-05 10:25:23.000000',1,'2017-12-05 10:25:23.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(23,'PR201712050017',1207,1,0,'2017-12-05 10:27:40.000000',1,'2017-12-05 10:27:40.000000',2,1,1211,'2017-12-04 16:00:00.000000','',107),(24,'PR201712050018',1207,1,1,'2017-12-06 08:03:52.000000',1,'2017-12-05 10:29:11.000000',2,1,1213,'2017-12-06 08:03:52.000000','',111);
/*!40000 ALTER TABLE `t_order_apply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_order_apply_rec`
--

DROP TABLE IF EXISTS `t_order_apply_rec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_order_apply_rec` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `c_order_apply` int(10) NOT NULL,
  `c_goods` int(10) NOT NULL,
  `c_qty` decimal(8,3) NOT NULL,
  `c_use` varchar(255) NOT NULL,
  `c_supplier` int(10) NOT NULL,
  `c_time_arrive` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `c_close` bit(1) NOT NULL,
  `c_qty_to` decimal(8,3) NOT NULL,
  `c_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `PK_T_ORDER_APPLY_REC` (`c_id`),
  KEY `idx_order_apply_rec_master` (`c_order_apply`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_order_apply_rec`
--

LOCK TABLES `t_order_apply_rec` WRITE;
/*!40000 ALTER TABLE `t_order_apply_rec` DISABLE KEYS */;
INSERT INTO `t_order_apply_rec` VALUES (1,24,4,1.000,'',0,'2017-12-05 10:30:13.000000','\0',0.000,''),(3,24,4,5.000,'',0,'2017-12-05 16:00:00.000000','',5.000,''),(5,24,6,1.000,'',0,'2017-12-06 02:44:16.000000','',1.000,''),(6,24,5,1.000,'',0,'2017-12-06 02:47:51.000000','',1.000,'');
/*!40000 ALTER TABLE `t_order_apply_rec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_order_rec`
--

DROP TABLE IF EXISTS `t_order_rec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_order_rec` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `c_order` int(10) NOT NULL,
  `c_goods` int(10) NOT NULL,
  `c_unit` int(10) NOT NULL,
  `c_qty` decimal(8,3) NOT NULL,
  `c_price` decimal(12,6) NOT NULL,
  `c_price_tax` decimal(12,6) NOT NULL,
  `c_discount` decimal(5,2) DEFAULT NULL,
  `c_tax` decimal(5,2) NOT NULL,
  `c_date_delivery` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `c_memo` varchar(255) NOT NULL,
  `c_qty_from` decimal(8,3) NOT NULL,
  `c_qty_to` decimal(8,3) NOT NULL,
  `c_rec_from` int(10) NOT NULL,
  `c_no_from` varchar(20) NOT NULL,
  `c_qty_kp` decimal(8,3) NOT NULL,
  `c_close` bit(1) NOT NULL,
  `c_qty_stock` decimal(8,3) NOT NULL,
  `c_is_pay` bit(1) NOT NULL,
  `c_amt` decimal(12,2) NOT NULL,
  `c_amt_tax` decimal(12,2) NOT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `PK_T_ORDER_REC` (`c_id`),
  KEY `idx_order_rec_master` (`c_order`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_order_rec`
--

LOCK TABLES `t_order_rec` WRITE;
/*!40000 ALTER TABLE `t_order_rec` DISABLE KEYS */;
INSERT INTO `t_order_rec` VALUES (2,1,5,1224,1.000,0.000000,0.000000,NULL,17.00,'2017-12-06 08:06:30.000000','',1.000,1.000,6,'PR201712050018',0.000,'',0.000,'\0',0.00,0.00),(3,1,4,1224,5.000,0.000000,0.000000,NULL,17.00,'2017-12-06 08:06:32.000000','',5.000,5.000,3,'PR201712050018',0.000,'',0.000,'\0',0.00,0.00),(4,2,6,688,1.000,0.000000,0.000000,NULL,17.00,'2017-12-06 08:33:28.000000','',1.000,1.000,5,'PR201712050018',0.000,'',0.000,'\0',0.00,0.00);
/*!40000 ALTER TABLE `t_order_rec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_pay`
--

DROP TABLE IF EXISTS `t_pay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_pay` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `c_no` varchar(20) NOT NULL,
  `c_type` int(10) NOT NULL,
  `c_pay_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `c_supplier` int(10) NOT NULL,
  `c_amt_should` decimal(12,2) NOT NULL,
  `c_amt_actual` decimal(12,2) NOT NULL,
  `c_way` int(10) NOT NULL,
  `c_pay_type` int(10) NOT NULL,
  `c_manager` int(10) NOT NULL,
  `c_salesman` int(10) NOT NULL,
  `c_jizhang` int(10) NOT NULL,
  `c_dept` int(10) NOT NULL,
  `c_status` int(10) NOT NULL,
  `c_appr_people` int(10) NOT NULL,
  `c_appr_time` timestamp(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000',
  `c_no_order` varchar(255) NOT NULL,
  `c_no_order2` varchar(255) NOT NULL,
  `c_user` int(10) NOT NULL,
  `c_time` timestamp(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000',
  `c_memo` varchar(255) NOT NULL,
  `c_group` int(10) NOT NULL,
  `c_act` int(10) DEFAULT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `PK_T_PAY` (`c_id`),
  KEY `idx_pay_supplier` (`c_supplier`),
  KEY `idx_pay_time` (`c_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_pay`
--

LOCK TABLES `t_pay` WRITE;
/*!40000 ALTER TABLE `t_pay` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_pay_rec`
--

DROP TABLE IF EXISTS `t_pay_rec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_pay_rec` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `c_pay` int(10) NOT NULL,
  `c_goods` int(10) NOT NULL,
  `c_id_order` varchar(20) NOT NULL,
  `c_id_bill` decimal(8,3) NOT NULL,
  `c_no_bill` varchar(20) NOT NULL,
  `c_amt_should` decimal(12,2) NOT NULL,
  `c_amt_actual` decimal(12,2) NOT NULL,
  `c_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `PK_T_PAY_REC` (`c_id`),
  KEY `idx_pay_rec_master` (`c_pay`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_pay_rec`
--

LOCK TABLES `t_pay_rec` WRITE;
/*!40000 ALTER TABLE `t_pay_rec` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pay_rec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_period`
--

DROP TABLE IF EXISTS `t_period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_period` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `c_stock` int(10) NOT NULL,
  `c_time_begin` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `c_time_end` timestamp(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000',
  `c_begin` decimal(12,2) NOT NULL,
  `c_in` decimal(12,2) NOT NULL,
  `c_out` decimal(12,2) NOT NULL,
  `c_end` decimal(12,2) NOT NULL,
  `c_status` int(10) NOT NULL,
  `c_user` int(10) NOT NULL,
  `c_time` timestamp(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000',
  `c_memo` varchar(255) NOT NULL,
  `c_group` int(10) NOT NULL,
  `c_period_ucode` int(10) NOT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `PK_T_PERIOD` (`c_id`),
  KEY `idx_period_stock` (`c_stock`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_period`
--

LOCK TABLES `t_period` WRITE;
/*!40000 ALTER TABLE `t_period` DISABLE KEYS */;
INSERT INTO `t_period` VALUES (2,664,'2017-12-16 16:00:00.000000','2017-12-16 16:00:00.000000',0.00,0.00,0.00,0.00,0,1,'2017-12-17 05:59:07.000000','',2,1278);
/*!40000 ALTER TABLE `t_period` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_period_goods`
--

DROP TABLE IF EXISTS `t_period_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_period_goods` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `c_period` int(10) NOT NULL,
  `c_stock` int(10) NOT NULL,
  `c_goods` int(10) NOT NULL,
  `c_begin` decimal(18,2) NOT NULL,
  `c_begin_qty` decimal(8,3) NOT NULL,
  `c_begin_price` decimal(12,6) NOT NULL,
  `c_in_qty` decimal(8,3) NOT NULL,
  `c_in_price` decimal(12,6) NOT NULL,
  `c_in` decimal(18,2) NOT NULL,
  `c_out_qty` decimal(8,3) NOT NULL,
  `c_out_price` decimal(12,6) NOT NULL,
  `c_out` decimal(18,2) NOT NULL,
  `c_end_qty` decimal(8,3) NOT NULL,
  `c_end_price` decimal(12,6) NOT NULL,
  `c_end_qty_calc` decimal(8,3) NOT NULL,
  `c_end` decimal(18,2) NOT NULL,
  `c_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `idx_period_goods_stock` (`c_period`,`c_stock`,`c_goods`),
  UNIQUE KEY `PK_T_PERIOD_GOODS` (`c_id`),
  KEY `idx_period_goods_period` (`c_period`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_period_goods`
--

LOCK TABLES `t_period_goods` WRITE;
/*!40000 ALTER TABLE `t_period_goods` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_period_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_price_supplier`
--

DROP TABLE IF EXISTS `t_price_supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_price_supplier` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `c_supplier` int(10) NOT NULL,
  `c_goods` int(10) NOT NULL,
  `c_price` decimal(12,6) NOT NULL,
  `c_tax` decimal(5,2) NOT NULL,
  `c_price_tax` decimal(12,6) NOT NULL,
  `c_price_avg` decimal(12,6) NOT NULL,
  `c_memo` varchar(255) NOT NULL,
  `c_group` int(10) NOT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `PK_T_PRICE_SUPPLIER` (`c_id`),
  KEY `idx_price_supplier_goods` (`c_goods`),
  KEY `idx_price_supplier_supplier` (`c_supplier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_price_supplier`
--

LOCK TABLES `t_price_supplier` WRITE;
/*!40000 ALTER TABLE `t_price_supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_price_supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_stock_goods`
--

DROP TABLE IF EXISTS `t_stock_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_stock_goods` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `c_stock` int(10) NOT NULL,
  `c_goods` int(10) NOT NULL,
  `c_qty` decimal(8,3) NOT NULL,
  `c_qty_min` decimal(8,3) NOT NULL,
  `c_qty_max` decimal(8,3) NOT NULL,
  `c_price` decimal(12,6) NOT NULL,
  `c_user` int(10) NOT NULL,
  `c_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `c_memo` varchar(255) NOT NULL,
  `c_group` int(10) NOT NULL,
  `c_goods_status` int(10) DEFAULT NULL,
  `c_qty_way` decimal(8,3) DEFAULT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `idx_stock_goods` (`c_stock`,`c_goods`),
  UNIQUE KEY `PK_T_STOCK_GOODS` (`c_id`),
  KEY `idx_stock_goods_group` (`c_group`),
  KEY `idx_stock_goods_stock` (`c_stock`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_stock_goods`
--

LOCK TABLES `t_stock_goods` WRITE;
/*!40000 ALTER TABLE `t_stock_goods` DISABLE KEYS */;
INSERT INTO `t_stock_goods` VALUES (3,671,4,0.000,0.000,0.000,0.000000,1,'2017-12-14 09:18:03.677000','',2,1,0.000),(4,671,5,0.000,0.000,0.000,0.000000,1,'2017-12-14 09:18:03.693000','',2,1,0.000);
/*!40000 ALTER TABLE `t_stock_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_supplier`
--

DROP TABLE IF EXISTS `t_supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_supplier` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `c_no` varchar(20) NOT NULL,
  `c_name` varchar(255) NOT NULL,
  `c_address` varchar(255) NOT NULL,
  `c_status` int(10) NOT NULL,
  `c_industry` int(10) NOT NULL,
  `c_linkman` varchar(50) NOT NULL,
  `c_phone` varchar(20) NOT NULL,
  `c_mobile` varchar(20) NOT NULL,
  `c_fax` varchar(20) NOT NULL,
  `c_zip` varchar(20) NOT NULL,
  `c_email` varchar(50) NOT NULL,
  `c_bank_account` varchar(100) NOT NULL,
  `c_bank_code` varchar(20) NOT NULL,
  `c_registration_taxnum` varchar(20) NOT NULL,
  `c_vat` varchar(20) NOT NULL,
  `c_province` varchar(10) NOT NULL,
  `c_city` varchar(10) NOT NULL,
  `c_country` varchar(10) NOT NULL,
  `c_legal_person` varchar(20) NOT NULL,
  `c_discount` decimal(2,1) NOT NULL,
  `c_class` int(10) NOT NULL,
  `c_registered_trademark` varchar(50) NOT NULL,
  `c_business_licence` varchar(50) NOT NULL,
  `c_user` int(10) NOT NULL,
  `c_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `c_group` int(10) NOT NULL,
  `c_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `PK_T_SUPPLIER` (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_supplier`
--

LOCK TABLES `t_supplier` WRITE;
/*!40000 ALTER TABLE `t_supplier` DISABLE KEYS */;
INSERT INTO `t_supplier` VALUES (1,'0101','XXX供应商一','',0,786,'','','','','','','','','','','-1','-1','-1','',0.0,769,'','',1,'2017-12-06 22:55:51.000000',2,''),(2,'0102','XXX往来单位','',0,786,'','','','','','','','','','','150000','150400','150421','',0.0,769,'','',1,'2017-12-06 15:23:26.000000',2,'');
/*!40000 ALTER TABLE `t_supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `vw_docu_list`
--

DROP TABLE IF EXISTS `vw_docu_list`;
/*!50001 DROP VIEW IF EXISTS `vw_docu_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_docu_list` AS SELECT 
 1 AS `c_id`,
 1 AS `c_no`,
 1 AS `c_type`,
 1 AS `c_way`,
 1 AS `c_customer`,
 1 AS `c_supplier`,
 1 AS `c_stock`,
 1 AS `c_type_from`,
 1 AS `c_manager`,
 1 AS `c_salesman`,
 1 AS `c_jizhang`,
 1 AS `c_baoguan`,
 1 AS `c_user`,
 1 AS `c_time`,
 1 AS `c_dept`,
 1 AS `c_status`,
 1 AS `c_memo`,
 1 AS `c_is_back`,
 1 AS `c_group`,
 1 AS `c_appr_people`,
 1 AS `c_appr_time`,
 1 AS `c_no_from`,
 1 AS `c_kp_type`,
 1 AS `c_act`,
 1 AS `c_goods`,
 1 AS `c_ucode`,
 1 AS `c_name`,
 1 AS `c_desc`,
 1 AS `c_unit`,
 1 AS `c_class`,
 1 AS `c_pic`,
 1 AS `c_qty`,
 1 AS `c_tax`,
 1 AS `c_price`,
 1 AS `c_price_tax`,
 1 AS `c_amt`,
 1 AS `c_amt_tax`,
 1 AS `c_price_out`,
 1 AS `c_price_out_tax`,
 1 AS `c_amt_out`,
 1 AS `c_amt_out_tax`,
 1 AS `c_close`,
 1 AS `c_qty_kp`,
 1 AS `c_rec_from`,
 1 AS `c_no_order`,
 1 AS `c_pay_type`,
 1 AS `c_stock_to`,
 1 AS `bill_no`,
 1 AS `rec_id`,
 1 AS `c_period`,
 1 AS `c_qty_stock`,
 1 AS `c_qty_from`,
 1 AS `c_qty_to`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_docu_rec`
--

DROP TABLE IF EXISTS `vw_docu_rec`;
/*!50001 DROP VIEW IF EXISTS `vw_docu_rec`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_docu_rec` AS SELECT 
 1 AS `c_ucode`,
 1 AS `c_name`,
 1 AS `c_desc`,
 1 AS `c_class`,
 1 AS `c_pic`,
 1 AS `c_id`,
 1 AS `c_docu`,
 1 AS `c_goods`,
 1 AS `c_unit`,
 1 AS `c_qty`,
 1 AS `c_price`,
 1 AS `c_price_tax`,
 1 AS `c_tax`,
 1 AS `c_amt`,
 1 AS `c_amt_tax`,
 1 AS `c_memo`,
 1 AS `c_price_out`,
 1 AS `c_price_out_tax`,
 1 AS `c_amt_out`,
 1 AS `c_amt_out_tax`,
 1 AS `c_qty_from`,
 1 AS `c_qty_to`,
 1 AS `c_qty_stock`,
 1 AS `c_rec_from`,
 1 AS `c_no_from`,
 1 AS `c_no_order`,
 1 AS `c_qty_kp`,
 1 AS `c_close`,
 1 AS `c_supplier`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_order_apply_list`
--

DROP TABLE IF EXISTS `vw_order_apply_list`;
/*!50001 DROP VIEW IF EXISTS `vw_order_apply_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_order_apply_list` AS SELECT 
 1 AS `c_id`,
 1 AS `c_no`,
 1 AS `c_dept`,
 1 AS `c_way`,
 1 AS `c_appr_people`,
 1 AS `c_appr_time`,
 1 AS `c_applicant`,
 1 AS `c_applicat_time`,
 1 AS `c_group`,
 1 AS `c_user`,
 1 AS `c_status`,
 1 AS `c_time`,
 1 AS `c_memo`,
 1 AS `c_act`,
 1 AS `rec_id`,
 1 AS `c_ucode`,
 1 AS `c_name`,
 1 AS `c_desc`,
 1 AS `c_unit`,
 1 AS `c_class`,
 1 AS `c_pic`,
 1 AS `c_goods`,
 1 AS `c_qty`,
 1 AS `c_qty_to`,
 1 AS `c_close`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_order_apply_rec`
--

DROP TABLE IF EXISTS `vw_order_apply_rec`;
/*!50001 DROP VIEW IF EXISTS `vw_order_apply_rec`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_order_apply_rec` AS SELECT 
 1 AS `c_id`,
 1 AS `c_order_apply`,
 1 AS `c_goods`,
 1 AS `c_qty`,
 1 AS `c_use`,
 1 AS `c_supplier`,
 1 AS `c_time_arrive`,
 1 AS `c_close`,
 1 AS `c_qty_to`,
 1 AS `c_memo`,
 1 AS `c_ucode`,
 1 AS `c_name`,
 1 AS `c_desc`,
 1 AS `c_pic`,
 1 AS `c_unit`,
 1 AS `c_class`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_order_list`
--

DROP TABLE IF EXISTS `vw_order_list`;
/*!50001 DROP VIEW IF EXISTS `vw_order_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_order_list` AS SELECT 
 1 AS `c_id`,
 1 AS `c_no`,
 1 AS `c_type`,
 1 AS `c_way`,
 1 AS `c_supplier`,
 1 AS `c_type_from`,
 1 AS `c_addr_delivery`,
 1 AS `c_consignee`,
 1 AS `c_cash_type`,
 1 AS `c_currency`,
 1 AS `c_exchange_rate`,
 1 AS `c_manager`,
 1 AS `c_salesman`,
 1 AS `c_user`,
 1 AS `c_time`,
 1 AS `c_dept`,
 1 AS `c_act`,
 1 AS `c_status`,
 1 AS `c_memo`,
 1 AS `c_is_back`,
 1 AS `c_group`,
 1 AS `c_appr_people`,
 1 AS `c_appr_time`,
 1 AS `c_no_from`,
 1 AS `c_no_order`,
 1 AS `c_goods`,
 1 AS `c_ucode`,
 1 AS `c_name`,
 1 AS `c_desc`,
 1 AS `c_unit`,
 1 AS `c_class`,
 1 AS `c_pic`,
 1 AS `c_qty`,
 1 AS `c_amt`,
 1 AS `c_amt_tax`,
 1 AS `c_qty_stock`,
 1 AS `c_date_delivery`,
 1 AS `c_price_tax`,
 1 AS `c_close`,
 1 AS `c_qty_kp`,
 1 AS `rec_id`,
 1 AS `c_rec_from`,
 1 AS `c_qty_to`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_order_rec`
--

DROP TABLE IF EXISTS `vw_order_rec`;
/*!50001 DROP VIEW IF EXISTS `vw_order_rec`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_order_rec` AS SELECT 
 1 AS `c_id`,
 1 AS `c_order`,
 1 AS `c_goods`,
 1 AS `c_unit`,
 1 AS `c_qty`,
 1 AS `c_price`,
 1 AS `c_price_tax`,
 1 AS `c_discount`,
 1 AS `c_tax`,
 1 AS `c_date_delivery`,
 1 AS `c_memo`,
 1 AS `c_qty_from`,
 1 AS `c_qty_to`,
 1 AS `c_rec_from`,
 1 AS `c_no_from`,
 1 AS `c_qty_kp`,
 1 AS `c_close`,
 1 AS `c_qty_stock`,
 1 AS `c_is_pay`,
 1 AS `c_amt`,
 1 AS `c_amt_tax`,
 1 AS `c_ucode`,
 1 AS `c_name`,
 1 AS `c_desc`,
 1 AS `c_class`,
 1 AS `c_pic`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_pay_list`
--

DROP TABLE IF EXISTS `vw_pay_list`;
/*!50001 DROP VIEW IF EXISTS `vw_pay_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_pay_list` AS SELECT 
 1 AS `c_id`,
 1 AS `c_no`,
 1 AS `c_type`,
 1 AS `c_pay_time`,
 1 AS `c_supplier`,
 1 AS `c_amt_should`,
 1 AS `c_amt_actual`,
 1 AS `c_way`,
 1 AS `c_pay_type`,
 1 AS `c_manager`,
 1 AS `c_salesman`,
 1 AS `c_jizhang`,
 1 AS `c_dept`,
 1 AS `c_status`,
 1 AS `c_appr_people`,
 1 AS `c_appr_time`,
 1 AS `c_no_order`,
 1 AS `c_no_order2`,
 1 AS `c_user`,
 1 AS `c_time`,
 1 AS `c_memo`,
 1 AS `c_group`,
 1 AS `c_act`,
 1 AS `rec_id`,
 1 AS `c_goods`,
 1 AS `c_ucode`,
 1 AS `c_name`,
 1 AS `c_desc`,
 1 AS `c_class`,
 1 AS `c_unit`,
 1 AS `c_id_order`,
 1 AS `c_id_bill`,
 1 AS `c_no_bill`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_pay_rec`
--

DROP TABLE IF EXISTS `vw_pay_rec`;
/*!50001 DROP VIEW IF EXISTS `vw_pay_rec`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_pay_rec` AS SELECT 
 1 AS `c_ucode`,
 1 AS `c_name`,
 1 AS `c_desc`,
 1 AS `c_class`,
 1 AS `c_unit`,
 1 AS `c_id`,
 1 AS `c_pay`,
 1 AS `c_goods`,
 1 AS `c_id_order`,
 1 AS `c_id_bill`,
 1 AS `c_no_bill`,
 1 AS `c_amt_should`,
 1 AS `c_amt_actual`,
 1 AS `c_memo`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_period_class`
--

DROP TABLE IF EXISTS `vw_period_class`;
/*!50001 DROP VIEW IF EXISTS `vw_period_class`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_period_class` AS SELECT 
 1 AS `c_id`,
 1 AS `c_end`,
 1 AS `c_in`,
 1 AS `c_out`,
 1 AS `c_period_ucode`,
 1 AS `c_class`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_period_class_goods`
--

DROP TABLE IF EXISTS `vw_period_class_goods`;
/*!50001 DROP VIEW IF EXISTS `vw_period_class_goods`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_period_class_goods` AS SELECT 
 1 AS `c_id`,
 1 AS `c_period_ucode`,
 1 AS `c_stock`,
 1 AS `c_class`,
 1 AS `c_begin_qty`,
 1 AS `c_begin`,
 1 AS `c_in_qty`,
 1 AS `c_in`,
 1 AS `c_out_qty`,
 1 AS `c_out`,
 1 AS `c_end_qty`,
 1 AS `c_end`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_period_goods`
--

DROP TABLE IF EXISTS `vw_period_goods`;
/*!50001 DROP VIEW IF EXISTS `vw_period_goods`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_period_goods` AS SELECT 
 1 AS `c_id`,
 1 AS `c_period`,
 1 AS `c_stock`,
 1 AS `c_goods`,
 1 AS `c_begin`,
 1 AS `c_begin_qty`,
 1 AS `c_begin_price`,
 1 AS `c_in_qty`,
 1 AS `c_in_price`,
 1 AS `c_in`,
 1 AS `c_out_qty`,
 1 AS `c_out_price`,
 1 AS `c_out`,
 1 AS `c_end_qty`,
 1 AS `c_end_price`,
 1 AS `c_end_qty_calc`,
 1 AS `c_end`,
 1 AS `c_memo`,
 1 AS `c_period_ucode`,
 1 AS `c_time_begin`,
 1 AS `c_time_end`,
 1 AS `c_ucode`,
 1 AS `c_name`,
 1 AS `c_desc`,
 1 AS `c_unit`,
 1 AS `c_class`,
 1 AS `c_group`,
 1 AS `c_status`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_stock_goods`
--

DROP TABLE IF EXISTS `vw_stock_goods`;
/*!50001 DROP VIEW IF EXISTS `vw_stock_goods`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_stock_goods` AS SELECT 
 1 AS `c_id`,
 1 AS `c_stock`,
 1 AS `c_goods`,
 1 AS `c_qty`,
 1 AS `c_qty_way`,
 1 AS `c_qty_min`,
 1 AS `c_qty_max`,
 1 AS `c_memo`,
 1 AS `c_group`,
 1 AS `c_ucode`,
 1 AS `c_name`,
 1 AS `c_desc`,
 1 AS `c_unit`,
 1 AS `c_class`,
 1 AS `c_price`,
 1 AS `c_goods_status`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `vw_docu_list`
--

/*!50001 DROP VIEW IF EXISTS `vw_docu_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_docu_list` AS select `a`.`c_id` AS `c_id`,`a`.`c_no` AS `c_no`,`a`.`c_type` AS `c_type`,`a`.`c_way` AS `c_way`,`a`.`c_supplier` AS `c_customer`,`a`.`c_supplier` AS `c_supplier`,`a`.`c_stock` AS `c_stock`,`a`.`c_type_from` AS `c_type_from`,`a`.`c_manager` AS `c_manager`,`a`.`c_salesman` AS `c_salesman`,`a`.`c_jizhang` AS `c_jizhang`,`a`.`c_baoguan` AS `c_baoguan`,`a`.`c_user` AS `c_user`,`a`.`c_time` AS `c_time`,`a`.`c_dept` AS `c_dept`,`a`.`c_status` AS `c_status`,`a`.`c_memo` AS `c_memo`,`a`.`c_is_back` AS `c_is_back`,`a`.`c_group` AS `c_group`,`a`.`c_appr_people` AS `c_appr_people`,`a`.`c_appr_time` AS `c_appr_time`,`b`.`c_no_from` AS `c_no_from`,`a`.`c_kp_type` AS `c_kp_type`,`a`.`c_act` AS `c_act`,`b`.`c_goods` AS `c_goods`,`b`.`c_ucode` AS `c_ucode`,`b`.`c_name` AS `c_name`,`b`.`c_desc` AS `c_desc`,`b`.`c_unit` AS `c_unit`,`b`.`c_class` AS `c_class`,`b`.`c_pic` AS `c_pic`,`b`.`c_qty` AS `c_qty`,`b`.`c_tax` AS `c_tax`,`b`.`c_price` AS `c_price`,`b`.`c_price_tax` AS `c_price_tax`,`b`.`c_amt` AS `c_amt`,`b`.`c_amt_tax` AS `c_amt_tax`,`b`.`c_price_out` AS `c_price_out`,`b`.`c_price_out_tax` AS `c_price_out_tax`,`b`.`c_amt_out` AS `c_amt_out`,`b`.`c_amt_out_tax` AS `c_amt_out_tax`,`b`.`c_close` AS `c_close`,`b`.`c_qty_kp` AS `c_qty_kp`,`b`.`c_rec_from` AS `c_rec_from`,`b`.`c_no_order` AS `c_no_order`,`a`.`c_pay_type` AS `c_pay_type`,`a`.`c_stock_to` AS `c_stock_to`,`a`.`c_no_from` AS `bill_no`,`b`.`c_id` AS `rec_id`,`a`.`c_period` AS `c_period`,`b`.`c_qty_stock` AS `c_qty_stock`,`b`.`c_qty_from` AS `c_qty_from`,`b`.`c_qty_to` AS `c_qty_to` from (`t_docu` `a` join `vw_docu_rec` `b` on((`a`.`c_id` = `b`.`c_docu`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_docu_rec`
--

/*!50001 DROP VIEW IF EXISTS `vw_docu_rec`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_docu_rec` AS select `b`.`c_ucode` AS `c_ucode`,`b`.`c_name` AS `c_name`,`b`.`c_desc` AS `c_desc`,`b`.`c_class` AS `c_class`,`b`.`c_pic` AS `c_pic`,`a`.`c_id` AS `c_id`,`a`.`c_docu` AS `c_docu`,`a`.`c_goods` AS `c_goods`,`a`.`c_unit` AS `c_unit`,`a`.`c_qty` AS `c_qty`,`a`.`c_price` AS `c_price`,`a`.`c_price_tax` AS `c_price_tax`,`a`.`c_tax` AS `c_tax`,`a`.`c_amt` AS `c_amt`,`a`.`c_amt_tax` AS `c_amt_tax`,`a`.`c_memo` AS `c_memo`,`a`.`c_price_out` AS `c_price_out`,`a`.`c_price_out_tax` AS `c_price_out_tax`,`a`.`c_amt_out` AS `c_amt_out`,`a`.`c_amt_out_tax` AS `c_amt_out_tax`,`a`.`c_qty_from` AS `c_qty_from`,`a`.`c_qty_to` AS `c_qty_to`,`a`.`c_qty_stock` AS `c_qty_stock`,`a`.`c_rec_from` AS `c_rec_from`,`a`.`c_no_from` AS `c_no_from`,`a`.`c_no_order` AS `c_no_order`,`a`.`c_qty_kp` AS `c_qty_kp`,`a`.`c_close` AS `c_close`,`a`.`c_supplier` AS `c_supplier` from (`t_docu_rec` `a` join `t_goods` `b` on((`a`.`c_goods` = `b`.`c_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_order_apply_list`
--

/*!50001 DROP VIEW IF EXISTS `vw_order_apply_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_order_apply_list` AS select `a`.`c_id` AS `c_id`,`a`.`c_no` AS `c_no`,`a`.`c_dept` AS `c_dept`,`a`.`c_way` AS `c_way`,`a`.`c_appr_people` AS `c_appr_people`,`a`.`c_appr_time` AS `c_appr_time`,`a`.`c_applicant` AS `c_applicant`,`a`.`c_applicat_time` AS `c_applicat_time`,`a`.`c_group` AS `c_group`,`a`.`c_user` AS `c_user`,`a`.`c_status` AS `c_status`,`a`.`c_time` AS `c_time`,`a`.`c_memo` AS `c_memo`,`a`.`c_act` AS `c_act`,`b`.`c_id` AS `rec_id`,`b`.`c_ucode` AS `c_ucode`,`b`.`c_name` AS `c_name`,`b`.`c_desc` AS `c_desc`,`b`.`c_unit` AS `c_unit`,`b`.`c_class` AS `c_class`,`b`.`c_pic` AS `c_pic`,`b`.`c_goods` AS `c_goods`,`b`.`c_qty` AS `c_qty`,`b`.`c_qty_to` AS `c_qty_to`,`b`.`c_close` AS `c_close` from (`t_order_apply` `a` join `vw_order_apply_rec` `b`) where (`a`.`c_id` = `b`.`c_order_apply`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_order_apply_rec`
--

/*!50001 DROP VIEW IF EXISTS `vw_order_apply_rec`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_order_apply_rec` AS select `a`.`c_id` AS `c_id`,`a`.`c_order_apply` AS `c_order_apply`,`a`.`c_goods` AS `c_goods`,`a`.`c_qty` AS `c_qty`,`a`.`c_use` AS `c_use`,`a`.`c_supplier` AS `c_supplier`,`a`.`c_time_arrive` AS `c_time_arrive`,`a`.`c_close` AS `c_close`,`a`.`c_qty_to` AS `c_qty_to`,`a`.`c_memo` AS `c_memo`,`b`.`c_ucode` AS `c_ucode`,`b`.`c_name` AS `c_name`,`b`.`c_desc` AS `c_desc`,`b`.`c_pic` AS `c_pic`,`b`.`c_unit` AS `c_unit`,`b`.`c_class` AS `c_class` from (`t_order_apply_rec` `a` join `t_goods` `b`) where (`a`.`c_goods` = `b`.`c_id`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_order_list`
--

/*!50001 DROP VIEW IF EXISTS `vw_order_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_order_list` AS select `a`.`c_id` AS `c_id`,`a`.`c_no` AS `c_no`,`a`.`c_type` AS `c_type`,`a`.`c_way` AS `c_way`,`a`.`c_supplier` AS `c_supplier`,`a`.`c_type_from` AS `c_type_from`,`a`.`c_addr_delivery` AS `c_addr_delivery`,`a`.`c_consignee` AS `c_consignee`,`a`.`c_cash_type` AS `c_cash_type`,`a`.`c_currency` AS `c_currency`,`a`.`c_exchange_rate` AS `c_exchange_rate`,`a`.`c_manager` AS `c_manager`,`a`.`c_salesman` AS `c_salesman`,`a`.`c_user` AS `c_user`,`a`.`c_time` AS `c_time`,`a`.`c_dept` AS `c_dept`,`a`.`c_act` AS `c_act`,`a`.`c_status` AS `c_status`,`a`.`c_memo` AS `c_memo`,`a`.`c_is_back` AS `c_is_back`,`a`.`c_group` AS `c_group`,`a`.`c_appr_people` AS `c_appr_people`,`a`.`c_appr_time` AS `c_appr_time`,`a`.`c_no_from` AS `c_no_from`,`a`.`c_no_order` AS `c_no_order`,`b`.`c_goods` AS `c_goods`,`b`.`c_ucode` AS `c_ucode`,`b`.`c_name` AS `c_name`,`b`.`c_desc` AS `c_desc`,`b`.`c_unit` AS `c_unit`,`b`.`c_class` AS `c_class`,`b`.`c_pic` AS `c_pic`,`b`.`c_qty` AS `c_qty`,`b`.`c_amt` AS `c_amt`,`b`.`c_amt_tax` AS `c_amt_tax`,`b`.`c_qty_stock` AS `c_qty_stock`,`b`.`c_date_delivery` AS `c_date_delivery`,`b`.`c_price_tax` AS `c_price_tax`,`b`.`c_close` AS `c_close`,`b`.`c_qty_kp` AS `c_qty_kp`,`b`.`c_id` AS `rec_id`,`b`.`c_rec_from` AS `c_rec_from`,`b`.`c_qty_to` AS `c_qty_to` from (`t_order` `a` join `vw_order_rec` `b` on((`a`.`c_id` = `b`.`c_order`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_order_rec`
--

/*!50001 DROP VIEW IF EXISTS `vw_order_rec`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_order_rec` AS select `a`.`c_id` AS `c_id`,`a`.`c_order` AS `c_order`,`a`.`c_goods` AS `c_goods`,`a`.`c_unit` AS `c_unit`,`a`.`c_qty` AS `c_qty`,`a`.`c_price` AS `c_price`,`a`.`c_price_tax` AS `c_price_tax`,`a`.`c_discount` AS `c_discount`,`a`.`c_tax` AS `c_tax`,`a`.`c_date_delivery` AS `c_date_delivery`,`a`.`c_memo` AS `c_memo`,`a`.`c_qty_from` AS `c_qty_from`,`a`.`c_qty_to` AS `c_qty_to`,`a`.`c_rec_from` AS `c_rec_from`,`a`.`c_no_from` AS `c_no_from`,`a`.`c_qty_kp` AS `c_qty_kp`,`a`.`c_close` AS `c_close`,`a`.`c_qty_stock` AS `c_qty_stock`,`a`.`c_is_pay` AS `c_is_pay`,`a`.`c_amt` AS `c_amt`,`a`.`c_amt_tax` AS `c_amt_tax`,`b`.`c_ucode` AS `c_ucode`,`b`.`c_name` AS `c_name`,`b`.`c_desc` AS `c_desc`,`b`.`c_class` AS `c_class`,`b`.`c_pic` AS `c_pic` from (`t_order_rec` `a` join `t_goods` `b`) where (`a`.`c_goods` = `b`.`c_id`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_pay_list`
--

/*!50001 DROP VIEW IF EXISTS `vw_pay_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_pay_list` AS select `a`.`c_id` AS `c_id`,`a`.`c_no` AS `c_no`,`a`.`c_type` AS `c_type`,`a`.`c_pay_time` AS `c_pay_time`,`a`.`c_supplier` AS `c_supplier`,`a`.`c_amt_should` AS `c_amt_should`,`a`.`c_amt_actual` AS `c_amt_actual`,`a`.`c_way` AS `c_way`,`a`.`c_pay_type` AS `c_pay_type`,`a`.`c_manager` AS `c_manager`,`a`.`c_salesman` AS `c_salesman`,`a`.`c_jizhang` AS `c_jizhang`,`a`.`c_dept` AS `c_dept`,`a`.`c_status` AS `c_status`,`a`.`c_appr_people` AS `c_appr_people`,`a`.`c_appr_time` AS `c_appr_time`,`a`.`c_no_order` AS `c_no_order`,`a`.`c_no_order2` AS `c_no_order2`,`a`.`c_user` AS `c_user`,`a`.`c_time` AS `c_time`,`a`.`c_memo` AS `c_memo`,`a`.`c_group` AS `c_group`,`a`.`c_act` AS `c_act`,`b`.`c_id` AS `rec_id`,`b`.`c_goods` AS `c_goods`,`b`.`c_ucode` AS `c_ucode`,`b`.`c_name` AS `c_name`,`b`.`c_desc` AS `c_desc`,`b`.`c_class` AS `c_class`,`b`.`c_unit` AS `c_unit`,`b`.`c_id_order` AS `c_id_order`,`b`.`c_id_bill` AS `c_id_bill`,`b`.`c_no_bill` AS `c_no_bill` from (`t_pay` `a` join `vw_pay_rec` `b` on((`a`.`c_id` = `b`.`c_pay`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_pay_rec`
--

/*!50001 DROP VIEW IF EXISTS `vw_pay_rec`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_pay_rec` AS select `b`.`c_ucode` AS `c_ucode`,`b`.`c_name` AS `c_name`,`b`.`c_desc` AS `c_desc`,`b`.`c_class` AS `c_class`,`b`.`c_unit` AS `c_unit`,`a`.`c_id` AS `c_id`,`a`.`c_pay` AS `c_pay`,`a`.`c_goods` AS `c_goods`,`a`.`c_id_order` AS `c_id_order`,`a`.`c_id_bill` AS `c_id_bill`,`a`.`c_no_bill` AS `c_no_bill`,`a`.`c_amt_should` AS `c_amt_should`,`a`.`c_amt_actual` AS `c_amt_actual`,`a`.`c_memo` AS `c_memo` from (`t_pay_rec` `a` join `t_goods` `b` on((`a`.`c_goods` = `b`.`c_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_period_class`
--

/*!50001 DROP VIEW IF EXISTS `vw_period_class`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_period_class` AS select sum(`vw_period_goods`.`c_begin`) AS `c_id`,sum(`vw_period_goods`.`c_end`) AS `c_end`,sum(`vw_period_goods`.`c_in`) AS `c_in`,sum(`vw_period_goods`.`c_out`) AS `c_out`,`vw_period_goods`.`c_period_ucode` AS `c_period_ucode`,`vw_period_goods`.`c_class` AS `c_class` from `vw_period_goods` group by `vw_period_goods`.`c_period_ucode`,`vw_period_goods`.`c_class` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_period_class_goods`
--

/*!50001 DROP VIEW IF EXISTS `vw_period_class_goods`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_period_class_goods` AS select `vw_period_goods`.`c_period` AS `c_id`,`vw_period_goods`.`c_period_ucode` AS `c_period_ucode`,`vw_period_goods`.`c_stock` AS `c_stock`,`vw_period_goods`.`c_class` AS `c_class`,sum(`vw_period_goods`.`c_begin_qty`) AS `c_begin_qty`,sum(`vw_period_goods`.`c_begin`) AS `c_begin`,sum(`vw_period_goods`.`c_in_qty`) AS `c_in_qty`,sum(`vw_period_goods`.`c_in`) AS `c_in`,sum(`vw_period_goods`.`c_out_qty`) AS `c_out_qty`,sum(`vw_period_goods`.`c_out`) AS `c_out`,sum(`vw_period_goods`.`c_end_qty`) AS `c_end_qty`,sum(`vw_period_goods`.`c_end`) AS `c_end` from `vw_period_goods` group by `vw_period_goods`.`c_class`,`vw_period_goods`.`c_period_ucode`,`vw_period_goods`.`c_stock`,`vw_period_goods`.`c_period` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_period_goods`
--

/*!50001 DROP VIEW IF EXISTS `vw_period_goods`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_period_goods` AS select `a`.`c_id` AS `c_id`,`a`.`c_period` AS `c_period`,`a`.`c_stock` AS `c_stock`,`a`.`c_goods` AS `c_goods`,`a`.`c_begin` AS `c_begin`,`a`.`c_begin_qty` AS `c_begin_qty`,`a`.`c_begin_price` AS `c_begin_price`,`a`.`c_in_qty` AS `c_in_qty`,`a`.`c_in_price` AS `c_in_price`,`a`.`c_in` AS `c_in`,`a`.`c_out_qty` AS `c_out_qty`,`a`.`c_out_price` AS `c_out_price`,`a`.`c_out` AS `c_out`,`a`.`c_end_qty` AS `c_end_qty`,`a`.`c_end_price` AS `c_end_price`,`a`.`c_end_qty_calc` AS `c_end_qty_calc`,`a`.`c_end` AS `c_end`,`a`.`c_memo` AS `c_memo`,`d`.`c_period_ucode` AS `c_period_ucode`,`d`.`c_time_begin` AS `c_time_begin`,`d`.`c_time_end` AS `c_time_end`,`c`.`c_ucode` AS `c_ucode`,`c`.`c_name` AS `c_name`,`c`.`c_desc` AS `c_desc`,`c`.`c_unit` AS `c_unit`,`c`.`c_class` AS `c_class`,`d`.`c_group` AS `c_group`,`d`.`c_status` AS `c_status` from ((`t_period_goods` `a` join `t_goods` `c`) join `t_period` `d`) where ((`a`.`c_goods` = `c`.`c_id`) and (`a`.`c_period` = `d`.`c_id`) and (`c`.`c_type` = 0) and (`d`.`c_status` <> -(1))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_stock_goods`
--

/*!50001 DROP VIEW IF EXISTS `vw_stock_goods`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_stock_goods` AS select `a`.`c_id` AS `c_id`,`a`.`c_stock` AS `c_stock`,`a`.`c_goods` AS `c_goods`,`a`.`c_qty` AS `c_qty`,`a`.`c_qty_way` AS `c_qty_way`,`a`.`c_qty_min` AS `c_qty_min`,`a`.`c_qty_max` AS `c_qty_max`,`a`.`c_memo` AS `c_memo`,`a`.`c_group` AS `c_group`,`c`.`c_ucode` AS `c_ucode`,`c`.`c_name` AS `c_name`,`c`.`c_desc` AS `c_desc`,`c`.`c_unit` AS `c_unit`,`c`.`c_class` AS `c_class`,`a`.`c_price` AS `c_price`,`a`.`c_goods_status` AS `c_goods_status` from (`t_stock_goods` `a` join `t_goods` `c` on((`a`.`c_goods` = `c`.`c_id`))) where (`c`.`c_type` = 0) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-15 17:39:05
